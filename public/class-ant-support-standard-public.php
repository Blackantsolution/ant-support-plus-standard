<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 * @author     Your Name <email@example.com>
 */
class Wordpress_support_plus_standard_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/class-ant-support-standard-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script($this->plugin_name.'-ckeditor',plugin_dir_url(__FILE__).'ckeditor/ckeditor.js');
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/class-ant-support-standard-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'woocommerce' . '-react', plugin_dir_url( __FILE__ ) . 'js/react.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'woocommerce' . '-react-dom', plugin_dir_url( __FILE__ ) . 'js/react-dom.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-tickets', plugin_dir_url( __FILE__ ) . 'js/components/wooc-tickets.js', array( 'jquery' ), $this->version, false );
		wp_register_script( 'aspf_support_plus_standard', '' );
		$user=wp_get_current_user(  );
		$useremail=$user->user_email;
		// Localize the script with new data
		 $array = array(
		   'ajax_url' => admin_url('admin-ajax.php'),
		   'site_url'=>site_url('/'),
			'nonce'=>wp_create_nonce("wp_rest"),
			'customer_id'=>$useremail
		);
		wp_localize_script( 'aspf_support_plus_standard', 'ant_support_plus_standard_public', $array );

		// Enqueued script with localized data.
		wp_enqueue_script( 'aspf_support_plus_standard');		
	}
	function prefix_register_my_comment_route(){
		 
		register_rest_route( $this->plugin_name.'/v1','/blackant/tablefields',array(
			'methods'=> WP_REST_Server::READABLE,
			'callback' =>array($this,'gettablefields'),
		));
		register_rest_route( $this->plugin_name.'/v1','/blackant/customertickets',array(
			'methods'=> WP_REST_Server::READABLE,
			'callback' =>array($this,'getTickets'),
			
		));
		register_rest_route( $this->plugin_name.'/v1','/blackant/ticketpriority',array(
			'methods'=> WP_REST_Server::READABLE,
			'callback' =>array($this,'getTicketpriority'),
			
		));
	}
	 
	/**
	 * 
	 * Return table fields
	 */
	function gettablefields(){
		$fields=['subject','Ticket status','Ticket priority'];
		return rest_ensure_response( $fields,200 );
		
	}
	/**
	 * 
	 * Return current user tickets
	 */
	function getTickets($data){
		 global $wpdb;
		 $customer_id=base64_decode($data['customer_id']);
		$results=$wpdb->get_results("SELECT * FROM {$wpdb->prefix}ant_tickets where customer_id='$customer_id'");
		return rest_ensure_response( $results );
	}
	/**
	 * 
	 * Return Ticket priorities
	 */
	function getTicketpriority(){
	global $wpdb;
	$priority=$wpdb->get_results("SELECT * FROM {$wpdb->prefix}ant_ticket_priorities");
	return rest_ensure_response( $priority );
	}
	/**
	 * 
	 * Return Ticket details using ticket id
	 */
	function ticketlistview(){
		global $wpdb;
		$ticketid=$_REQUEST['ticketid'];
		$sql=$wpdb->get_results("SELECT * FROM {$wpdb->prefix}ant_tickets where id='$ticketid'");
		echo json_encode($sql);
		wp_die();
	}
	/**
	 * 
	 * Create New tickets
	 */
	function create_ticket(){
		global $wpdb;
		$ticket_subjct=$_REQUEST['ticket_subject'];
		$ticket_desc=$_REQUEST['ticket_des'];
		$anttickets=$wpdb->prefix."ant_tickets";
		$duedate=date('Y-m-d', strtotime("+2 days"));
		$general_option = get_option('general_option');
		$ticket_prefix=$general_option['ticket_prefix'];
		$initial_id=$general_option['initialid']+1;
		$general_option['initialid']=$initial_id;
//	print_R($general_option);die;
			$current_user=wp_get_current_user(  );
			$user_email=$current_user->user_email;
			$user_name=$current_user->user_login;
			update_option('general_option',$general_option);
			$ticket_id=$ticket_prefix.$initial_id;
//	print_R($_REQUEST);die;
	//	$wpdb->show_errors();
		$sql=$wpdb->insert(
			$anttickets,
			array(
				'ticket_id'=>$ticket_id,
				'subject'=>$ticket_subjct,
				'description'=>$ticket_desc,
				'ticket_priority'=>$_REQUEST['ticket_priority'],
				'ticket_status'=>'Open',
				'ticket_category'=>'General',	
				'customer_id'=>$user_email,
				'ticket_source'=>'Form',
				'created_by'=>$user_name,
				'due_date'=>$duedate,
				'creator_type'=>'customer',
			//	'assigned_to'=>,
				'created_at'=>current_time('mysql'),
				'updated_at'=>current_time('mysql')
			)
			);
			echo json_encode($wpdb->get_results("select * from $anttickets  order by id desc limit 1"));
		//	$wpdb->print_error();
		wp_die();
	}
	//Add replies to ticket
	function replyticket(){
		global $wpdb;
		$replies_ticket=$wpdb->prefix."ant_ticket_replies";
		$wpdb->show_errors();
		$sql=$wpdb->insert($replies_ticket,
		array('ticket_id'=>$_REQUEST['ticket_id'],
		'description'=>$_REQUEST['replyContent'],
		'creator_type'=>$_REQUEST['creator_type'],
		'created_by'=>$_REQUEST['created_by']
	));
		$wpdb->print_error();
		wp_die();
	}
	function uploadfiles(){
		//	print_r($_FILES);
			$maxfiles=get_option('maxfiles');
			 
			if (!empty($_FILES['file']['name'][0] || $maxfiles<=2)) {
	
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
		
				$allowed_file_types = array( "image/jpeg", "image/jpg", "image/png","application/doc",'application/pdf','application/xls');
				$files = $_FILES['file'];
				$count = 0;
				$galleryImages = array();
			$date=date("Y-m-d h:i:sa");
	
				foreach ($files['name'] as $count => $value) {
		
					if ($files['name'][$count] && $files['size'][$count] <=' 2000000' && in_array($files['type'][$count],$allowed_file_types)) {
						 
						$file = array(
							'name'     => $files['name'][$count],
							'type'     => $files['type'][$count],
							'tmp_name' => $files['tmp_name'][$count],
							'error'    => $files['error'][$count],
							'size'     => $files['size'][$count]
						);
		
						$upload_overrides = array( 'test_form' => false );
						$upload = wp_handle_upload($file, $upload_overrides);
		
						// $filename should be the path to a file in the upload directory.
						$filename = $upload['file'];
	
						// The ID of the post this attachment is for.
						$parent_post_id = $post_id;
		
						// Check the type of tile. We'll use this as the 'post_mime_type'.
						$filetype = wp_check_filetype( basename( $filename ), null );
					
						// Get the path to the upload directory.
						$wp_upload_dir = wp_upload_dir();
		
						// Prepare an array of post data for the attachment.
						$attachment = array(
							'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
							'post_mime_type' => $filetype['type'],
							'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
							'post_content'   => '',
							'post_status'    => 'inherit'
						);
						
						// Insert the attachment.
						$attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );
		
						// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
						require_once( ABSPATH . 'wp-admin/includes/image.php' );
		
						// Generate the metadata for the attachment, and update the database record.
						$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
						wp_update_attachment_metadata( $attach_id, $attach_data );
		
						array_push($galleryImages, $attach_id);
						global $wpdb;
						//insert attachments;
						$attachment=$wpdb->prefix."ant_ticket_attachments";
					//	$wpdb->show_errors();
						$wpdb->query( $wpdb->prepare( 
							"
								INSERT INTO $attachment
								(attachment_name,attachment_type,attachment_path,created_at,updated_at)
								VALUES (%s,%s,%s,%s,%s)
							", 
						$files['name'][$count],
						$files['type'][$count],
						$files['tmp_name'][$count],
						$date,
						$date
						) );
					//	$wpdb->print_error();
		
					}
					else
					{
						echo  $upload_errors .= '<p>Invalid file type: ' . $_FILES['file']['type'] . '. Supported file types: jpg, jpeg, png, doc, pdf</p>';
					}
					$count++;
		
					// add images to the gallery field
				//	update_field('field_535e6a644107b', $galleryImages, $post_id);
		
				}
		
			}
			echo "file uploaded";
			wp_die();
		}
		function getcurrentuser(){
			global $wpdb;
			if(!defined("REST_REQUEST")){
			$user=wp_get_current_user( );
			$user_tickets=$wpdb->get_results("SELECT * FROM {$wpdb->prefix}ant_tickets WHERE customer_id='$user->user_email'");
			$serializedvalue=json_encode($user_tickets);
		//	$filter=apply_filters( 'the_excerpt', $serializedvalue );
		//	echo "<div><input type='hidden' id='currentuser_tickets' value='$serializedvalue' /></div>";
			//return $user_tickets; 
			}
		}
		
}
