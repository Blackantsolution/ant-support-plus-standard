(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	jQuery(document).on('click','#listview',function(e){
		e.preventDefault();
		jQuery('#customer_tickets').hide();
		jQuery('.aspf-addticket').hide();
		var ticketid=jQuery(this).attr('ticket_id');
		var data={
			action:"ticketlistview",
			ticketid:ticketid
		}
		jQuery.post(ant_support_plus_standard_public.ajax_url,data,function(response){
			var api_data=JSON.parse(response)
			var detailview='<div class="aspf-detailview aspf-bg-white aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg"><div class="aspf-px-4 aspf-py-5 aspf-border-b aspf-border-gray-200 aspf-sm:px-6">  <h3 class="aspf-text-lg aspf-leading-6 aspf-font-medium aspf-text-gray-900">	Tickets Information  </h3></div><div>  <dl class="aspf--mt-4"> <div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Subject </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+api_data[0].subject+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Ticket Status </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+api_data[0].ticket_status+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Ticket priority </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+api_data[0].ticket_priority+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Description </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+api_data[0].description+'</dd></div>	</dl></div></div><div><textarea id="replytoticket" name="replytoticket" /></div><div><button class="replysave" ticket_id='+api_data[0].id+' created_by='+api_data[0].created_by+' creator_type='+api_data[0].creator_type+'>Reply</button>&nbsp;<button class="canceldetailview">Back</button></div>';
		//	console.log(detailview)
		//	CKEDITOR.replace('replytoticket')
			jQuery('#ant_tickets').append(detailview)
		})
	})
	jQuery(document).on('click','.canceldetailview',function(){
		window.location.href=window.location.href;
	})

	//Cretate New Ticket
	var click=1;
	jQuery(document).on('click','.aspf-addticket',function(){
		if(click == 1){
		CKEDITOR.replace('ant_standard_publicticketdescription');
		click++;
		}
		jQuery(this).hide();
		jQuery('#customer_tickets').hide();
		jQuery('.ticketsubmit').show();
	})
	//click submit new tickets action
	jQuery(document).on('click','.submit_public_ticket',function(){
		var ticket_subject=jQuery('#ticketsubject').val();
		var username=jQuery(this).attr('username');
		var useremail=jQuery(this).attr('useremail');
		var ticket_priority=jQuery('#ant-ticketpriority option:selected').text();
		var ticket_des=CKEDITOR.instances.ant_standard_publicticketdescription.getData();
		if(ticket_subject == ''){
			jQuery('#ticketsubject').focus();
			 jQuery('#ticketsubject').effect("shake", { times:3 }, 300);
			jQuery("<p style='color:red'>Please Enter a subject</p>").insertAfter('#ticketsubject');
			return false;
		}
		var data={
			action:"aspflogin",
			ticket_subject:ticket_subject,
			ticket_des:ticket_des,
			username:username,
			useremail:useremail,
			ticket_priority:ticket_priority,
			'type':'ticketsubmission'
		}
		jQuery.post(ant_support_plus_public.ajax_url,data,function(response){
			var Obj=JSON.parse(response)
			//	console.log(Obj);return false
				jQuery('#customer_tickets tbody').append('<tr><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><div class="aspf-text-2xl aspf-leading-5 aspf-text-gray-900"><a href="#" id="ticketlistview" ticketid='+Obj[0].id+'>'+Obj[0].subject+'</a></div></td><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><div class="aspf-text-2xl aspf-leading-5 aspf-text-gray-900">'+Obj[0].ticket_status+'</div></td><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><div class="aspf-text-2xl aspf-leading-5 aspf-text-gray-900">'+Obj[0].ticket_priority+'</div></td></tr>');
					var detailview='<div class="aspf-detailview aspf-bg-white aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg"><div class="aspf-px-4 aspf-py-5 aspf-border-b aspf-border-gray-200 aspf-sm:px-6">  <h3 class="aspf-text-lg aspf-leading-6 aspf-font-medium aspf-text-gray-900">	Tickets Information  </h3></div><div>  <dl class="aspf--mt-4"> <div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Subject </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+Obj[0].subject+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Ticket Status </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+Obj[0].ticket_status+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Ticket priority </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+Obj[0].ticket_priority+'</dd></div>	<div class="aspf-bg-gray-50 aspf-px-4 aspf-py-5 aspf-sm:grid aspf-sm:grid-cols-3 aspf-sm:gap-4 aspf-sm:px-6"><dt class="aspf-text-3xl aspf-leading-5 aspf-font-medium aspf-text-gray-500">Description </dt><dd class="aspf-mt-10 aspf-text-3xl aspf-leading-5 aspf-text-gray-900 aspf-sm:mt-0 aspf-sm:col-span-2 aspf-text-center aspf--mt-4 ">'+Obj[0].description+'</dd></div></dl></div></div><div><textarea id="replytoticket" name="replytoticket" /></div><div><button class="replysave" ticket_id='+Obj[0].id+' created_by='+Obj[0].created_by+' creator_type='+Obj[0].creator_type+'>Reply</button>&nbsp;<button class="canceldetailview">Back</button></div>';
				//	console.log(detailview)
		//	CKEDITOR.replace('replytoticket');
			jQuery('#ant_tickets').append(detailview)
		//	jQuery('.ant-public-ticket').show();
		//	jQuery('.public_tickets_table').show();
			jQuery('.ticketsubmit').hide();
		//	console.log('Data saved');
		})
	})
	//reply to ticket 
	jQuery(document).on('click','.replysave',function(){

		var replyContent=jQuery('textarea#replytoticket').val();
		var ticket_id=jQuery(this).attr('ticket_id');
		var created_by=jQuery(this).attr('created_by');
		var creator_type=jQuery(this).attr('creator_type');
		var data={
			action:'replyticket',
			replyContent:replyContent,
			created_by:created_by,
			creator_type:creator_type,
			ticket_id:ticket_id
		}
		jQuery.post(ant_support_plus_public.ajax_url,data,function(reponse){
			console.log("Reply saved");
			window.location.href=window.location.href;
		})
	})
	//cancel public ticket submission
	jQuery(document).on('click','.cancel_public_ticket',function(){
		jQuery('.aspf-addticket').show();
		jQuery('#customer_tickets').show();
		jQuery('.ticketsubmit').hide();
	})
	$(document).on('click','#upload',function(){
		var formData=new FormData(document.getElementById('settingsform'));
		var inputValue = formData.get("file");
		var jsonstr=JSON.stringify(inputValue);
		var file=new Blob([inputValue],{"type": "text/html"});
	//	console.log(file)	 
		 formData.append("action", "uploadfiles");   
		 jQuery.ajax({
			 action:'uploadfiles',
            url:ajax_var.url,
            type: 'POST',
	   		 dataType:'json',
            data: formData, 
            cache: false,
            processData: false, 
            contentType: false,     
            success: function(data) {
            alert("saved");
            }
        });
	

	 })
})( jQuery );
