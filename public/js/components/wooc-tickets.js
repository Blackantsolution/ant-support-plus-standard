'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function TicketsListview() {
  var aspf_endpoint = ant_support_plus_standard_public.site_url + 'wp-json/ant-support-plus-standard/v1/blackant';
  var customer_id = btoa(ant_support_plus_standard_public.customer_id);

  var _React$useState = React.useState([]),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      tablefields = _React$useState2[0],
      setTableField = _React$useState2[1];

  var _React$useState3 = React.useState([]),
      _React$useState4 = _slicedToArray(_React$useState3, 2),
      customertickets = _React$useState4[0],
      setCustomertikets = _React$useState4[1];

  var _React$useState5 = React.useState([]),
      _React$useState6 = _slicedToArray(_React$useState5, 2),
      priority = _React$useState6[0],
      setPriority = _React$useState6[1];

  React.useEffect(function () {
    if (Object.keys(tablefields).length == 0) {
      fetch(aspf_endpoint + "/tablefields").then(function (res) {
        return res.json();
      }).then(function (data) {
        setTableField(data);
      }).then(function () {
        if (Object.keys(customertickets).length == 0) {
          fetch(aspf_endpoint + "/customertickets?customer_id=" + customer_id).then(function (res) {
            return res.json();
          }).then(function (data) {
            //   console.log(data)
            setCustomertikets(data);
          });
        }
      }).then(function () {
        if (Object.keys(priority).length == 0) {
          fetch(aspf_endpoint + '/ticketpriority').then(function (res) {
            return res.json();
          }).then(function (data) {
            //  console.log(data)
            setPriority(data);
          }, function (error) {
            // Error occured
            console.log(error);
          });
        }
      });
    }
  });
  return React.createElement(
    'div',
    { className: 'aspf-my-2 aspf-py-2 aspf-overflow-x-auto aspf-sm:-mx-6 aspf-sm:px-6 aspf-lg:-mx-8 aspf-lg:px-8 ', id: 'ant_tickets' },
    React.createElement(
      'div',
      { className: 'aspf-align-middle aspf-inline-block aspf-min-w-full aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg aspf-border-b aspf-border-gray-200' },
      React.createElement(
        'button',
        { className: 'aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline aspf-float-right aspf-addticket', type: 'button' },
        'Add Ticket'
      ),
      React.createElement(
        'table',
        { className: 'aspf-min-w-full', id: 'customer_tickets' },
        React.createElement(
          'thead',
          null,
          React.createElement(
            'tr',
            null,
            tablefields.map(function (data, index) {
              return React.createElement(
                'th',
                { key: index, className: 'aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-2xl aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider' },
                data
              );
            })
          )
        ),
        React.createElement(
          'tbody',
          { className: 'bg-white filter_table' },
          customertickets.map(function (data, index) {
            //   console.log(data)
            return React.createElement(
              'tr',
              { key: index },
              React.createElement(
                'td',
                { className: 'aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200' },
                React.createElement(
                  'div',
                  { className: 'aspf-text-2xl aspf-leading-5 aspf-text-gray-900' },
                  React.createElement(
                    'a',
                    { href: '#', ticket_id: data.id, id: 'listview' },
                    data.subject
                  )
                )
              ),
              React.createElement(
                'td',
                { className: 'aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200' },
                React.createElement(
                  'div',
                  { className: 'aspf-text-2xl aspf-leading-5 aspf-text-gray-900' },
                  data.ticket_status
                )
              ),
              React.createElement(
                'td',
                { className: 'aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200' },
                React.createElement(
                  'div',
                  { className: 'aspf-text-2xl aspf-leading-5 aspf-text-gray-900' },
                  data.ticket_priority
                )
              )
            );
          })
        )
      )
    ),
    React.createElement(
      'form',
      { className: 'aspf-hidden aspf-bg-white aspf-shadow-md aspf-rounded aspf-px-8 aspf-pt-6 aspf-pb-8 aspf-mb-4 ticketsubmit' },
      React.createElement(
        'div',
        { className: 'aspf-mb-6' },
        React.createElement(
          'label',
          { className: 'aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold mb-2', htmlFor: 'subject' },
          'Subject'
        ),
        React.createElement('input', { className: 'aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded aspf-w-6/12 aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline', id: 'ticketsubject', type: 'text' })
      ),
      React.createElement(
        'div',
        { className: 'aspf-inline-block aspf-pt-4 aspf-relative aspf-w-64' },
        React.createElement(
          'label',
          { className: 'aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold mb-2', htmlFor: 'username' },
          'Ticket Priority'
        ),
        React.createElement(
          'select',
          { className: 'aspf-block aspf-appearance-none aspf-w-full aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 aspf-pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline', id: 'ant-ticketpriority' },
          priority.map(function (data, index) {
            return React.createElement(
              'option',
              { value: 'ba-sp-' + data.name, key: index },
              data.name
            );
          })
        )
      ),
      React.createElement('br', null),
      React.createElement(
        'div',
        { className: 'aspf-mb-6' },
        React.createElement(
          'label',
          { className: 'aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold aspf-mb-2', htmlFor: 'password' },
          'Description'
        ),
        React.createElement('textarea', { className: 'aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline', id: 'ant_standard_publicticketdescription', name: 'ant_standard_publicticketdescription' })
      ),
      React.createElement(
        'div',
        { className: 'aspf-mt-4 ' },
        React.createElement(
          'button',
          { className: 'submit_public_ticket aspf-bg-blue-500 aspf-hover:bg-blue-700 vtext-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline', type: 'button' },
          'Submit'
        ),
        '\xA0',
        React.createElement(
          'button',
          { className: 'cancel_public_ticket aspf-bg-blue-500 aspf-hover:bg-blue-700 vtext-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline ', type: 'button' },
          'Cancel'
        ),
        '\xA0'
      )
    )
  );
}
jQuery(document).ready(function () {
  var domContainer = document.querySelector('.woocommerce-MyAccount-content');
  var url = window.location.href.indexOf('tickets');
  if (domContainer && url > -1) {
    ReactDOM.render(React.createElement(TicketsListview, null), domContainer);
  }
});