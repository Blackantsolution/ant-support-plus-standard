<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_Name
 *
 * @wordpress-plugin
 * Plugin Name:      Wordpress support plus standard
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Your Name or Your Company
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-name
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function wps_sup_std_activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ant-support-standard-activator.php';
	$activePlugins=get_option('active_plugins');
	if(in_array('woocommerce/woocommerce.php',$activePlugins)){
		Ant_Support_Standard_Activator::activate();
	}else{
		return false;
	}
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function wps_sup_std_deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ant-support-standard-deactivator.php';
	Ant_Support_Standard_Deactivator::deactivate();
}
register_activation_hook( __FILE__, 'wps_sup_std_activate_plugin_name' );
register_deactivation_hook( __FILE__, 'wps_sup_std_deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ant-support-standard.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name_std() {

	$plugin = new Wordpress_support_plus_free_standard();
	$plugin->run();

}
run_plugin_name_std();
