<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Ant_Support_Standard_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		global $wpdb;
		$table_name = $wpdb->prefix ."ant_custom_fields";
		$charset_collate = $wpdb->get_charset_collate();
	//	$wpdb->show_errors();
			$custom_field = "CREATE TABLE IF NOT EXISTS $table_name (
			`id` INT(10) NOT NULL AUTO_INCREMENT,
			`labels` VARCHAR(255) NOT NULL ,
			`sequence` INT(10) NOT NULL,
			`fields` VARCHAR(50),
			PRIMARY KEY (`id`)
) $charset_collate;";
dbDelta($custom_field);

		$table_name=$wpdb->prefix."ant_stand_template";

		$template_table="CREATE TABLE IF NOT EXISTS $table_name (
			`id` INT(10) NOT NULL AUTO_INCREMENT,
			`temp_labels` VARCHAR(255) NOT NULL ,
			`temp_content` VARCHAR(255),
			PRIMARY KEY (`id`)
		) $charset_collate;";
		dbDelta($template_table);

		$default_template=[
			['temp_labels'=>'Simple','temp_content'=>'This is a simple template'],
			['temp_labels'=>'Elegant','temp_content'=>'This is an elegant template'],
			 ];

			foreach($default_template as $key =>$value){
			//	print_R($value);
			//	$wpdb->show_errors();
				$wpdb->insert(
					$table_name,
					array(
						'temp_labels'=>$value['temp_labels'],
						'temp_content'=>$value['temp_content'],
					)
				);
		//	$wpdb->print_error();
			}
	}
}
