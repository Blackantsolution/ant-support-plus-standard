<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Wordpress_support_plus_free_standard {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Plugin_Name_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ANT_SUPPORT_PLUS_STANDARD_VERSION' ) ) {
			$this->version = ANT_SUPPORT_PLUS_STANDARD_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'ant-support-plus-standard';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Plugin_Name_Loader. Orchestrates the hooks of the plugin.
	 * - Plugin_Name_i18n. Defines internationalization functionality.
	 * - Plugin_Name_Admin. Defines all hooks for the admin area.
	 * - Plugin_Name_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-ant-support-standard-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-ant-support-standard-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-ant-support-standard-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-ant-support-standard-public.php';

		$this->loader = new Wordpress_support_plus_standard_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Plugin_Name_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Wordpress_support_plus_standard_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Wordpress_support_plus_standard_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action('admin_init',$plugin_admin,'loaded');
		$this->loader->add_action('custom',$plugin_admin,'custom');
		$this->loader->add_action( 'rest_api_init',$plugin_admin,'prefix_register_my_comment_route' );
		$this->loader->add_action('admin_init',$plugin_admin,'isa_activation');
		$this->loader->add_filter('cron_schedules',$plugin_admin,'my_cron_schedules');
		$this->loader->add_action( 'my_cron_schedules',$plugin_admin,'every_three_minutes_event_func');
		$this->loader->add_filter( 'woocommerce_account_menu_items',$plugin_admin,'my_custom_my_account_menu_items' );
		$this->loader->add_action( 'wp_loaded', $plugin_admin,'my_custom_flush_rewrite_rules' );
		$this->loader->add_filter( 'query_vars',$plugin_admin, 'my_custom_query_vars', 0 );
		$this->loader->add_action( 'init',$plugin_admin ,'my_custom_endpoints' );
		$this->loader->add_action('wp_ajax_imap_configure',$plugin_admin,'imap_configure');
		$this->loader->add_action('wp_ajax_customfieldvaluesave',$plugin_admin,'customfieldvaluesave');
		$this->loader->add_action('wp_ajax_cfupdatedragchanges',$plugin_admin,'cfupdatedragchanges');
		$this->loader->add_action('wp_ajax_customTemplates',$plugin_admin,'customTemplates');


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Wordpress_support_plus_standard_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action('wp_ajax_uploadfiles',$plugin_public,'uploadfiles');
		$this->loader->add_action( 'rest_api_init',$plugin_public,'prefix_register_my_comment_route' );
		$this->loader->add_action('init',$plugin_public,'getcurrentuser');
		$this->loader->add_action('wp_ajax_ticketlistview',$plugin_public,'ticketlistview');
		$this->loader->add_action('wp_ajax_nopriv_ticketlistview',$plugin_public,'ticketlistview');
		$this->loader->add_action('wp_ajax_create_ticket',$plugin_public,'create_ticket');
		$this->loader->add_action('wp_ajax_replyticket',$plugin_public,'replyticket');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
