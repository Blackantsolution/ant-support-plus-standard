<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Ant_Support_Standard_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
			global $wpdb;
			$standard_tables=['ant_custom_fields','ant_stand_template'];

			foreach($standard_tables as $ticket_table) {
				$table_name = $wpdb->prefix . $ticket_table;
				$wpdb->query("TRUNCATE TABLE {$table_name}");
			}
		
	}

}
