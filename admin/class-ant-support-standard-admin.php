<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class Wordpress_support_plus_standard_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( 'wps_sup_std', plugin_dir_url( __FILE__ ) . 'css/class-ant-support-standard-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
	//	wp_enqueue_script('wp-supdrag','https://code.jquery.com/jquery-1.12.4.js');
		wp_enqueue_script('wp-supdrop','https://code.jquery.com/ui/1.12.1/jquery-ui.js');
		wp_enqueue_script('wp_sup_lines','https://unpkg.com/babel-standalone@6/babel.min.js');
		wp_enqueue_script('ckeditor','https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js');
		wp_enqueue_script( 'wp-email',plugin_dir_url(__FILE__).'js/components/imap.js', array( 'jquery' ));
		wp_enqueue_script( $this->plugin_name . '-react', plugin_dir_url( __FILE__ ) . 'js/react.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-react-dom', plugin_dir_url( __FILE__ ) . 'js/react-dom.min.js', array( 'jquery' ), $this->version, false );

		wp_enqueue_script( 'wp_sup_plus_std', plugin_dir_url( __FILE__ ) . 'js/class-ant-support-standard-admin.js', array( 'jquery' ), $this->version, false );
		wp_localize_script($this->plugin_name, 'ant_support_standard_admin', array('url' => admin_url('admin-ajax.php'),'site_url' => site_url('/'),'nonce' => wp_create_nonce('wp_rest'),'home_url'=>home_url()));
	}
	function loaded(){

		$imapparams = [
			'imap' => ['label' => __('IMAP'), 'component' => 'ASPIMAP'],
		];

		$data = apply_filters( 'ant-support-plus-configuration', $imapparams );
		return $data;

	}
	function custom(){
	//	add_submenu_page( $parent_slug:string, $page_title:string, $menu_title:string, $capability:string, $menu_slug:string, $function:callable, $position:integer|null );
		add_submenu_page( 'list-table','imap', 'IMAP','manage_options','imap',array($this,'imap_settingsPage') );

	}
	function prefix_register_my_comment_route() {
		register_rest_route( $this->plugin_name.'/v1', '/blackant/imap', array(
			// By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
			'methods'  => WP_REST_Server::READABLE,
			// Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
			 'callback' =>array($this,'get_IMAP'),
			// 'permission_callback'=>array($this,'private_data_permission_check')
		) );
		register_rest_route( $this->plugin_name.'/v1', '/blackant/customfields', array(
			// By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
			'methods'  => WP_REST_Server::READABLE,
			// Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
			 'callback' =>array($this,'getCustomFields'),
			// 'permission_callback'=>array($this,'private_data_permission_check')
		) );
		register_rest_route( $this->plugin_name.'/v1', '/blackant/customtemplates', array(
			// By using this constant we ensure that when the WP_REST_Server changes our readable endpoints will work as intended.
			'methods'  => WP_REST_Server::READABLE,
			// Here we register our callback. The callback is fired when this endpoint is matched by the WP_REST_Server class.
			 'callback' =>array($this,'getCustomtemplates'),
			// 'permission_callback'=>array($this,'private_data_permission_check')
		) );

		register_rest_route($this->plugin_name.'/v1','/blackant/filter_ticketfield',array(
			'methods'=>'GET',
			'callback'=>array($this,'filter_ticketfield'),
		));
		}

		function imap_settingsPage(){
			echo "<div id='email'></div>";
			}
		/**
		 *
		 * Return IMAP cofiguration
		 */
		function get_IMAP(){

			$params = [
				'imap' => ['label' => __('IMAP'), 'component' => 'ASPIMAP'],
			];
			$data = apply_filters( 'ant-support-plus-configuration', $params );
			return new WP_REST_Response( $data, 200 );

	}
	 /***
	  *
	  *Return custom fields
	  */
	  function getCustomFields(){

			global $wpdb;
			$custom_fields_tables=$wpdb->prefix."ant_custom_fields";
			$fields_position_order=get_option('customfield_position_order');
		//	print_R($fields_position_order);die;
		 	//delete_option( 'customfield_position_order' );die;
			if(!$fields_position_order){
				$sql[]=$wpdb->get_results("select * from $custom_fields_tables ");			
			}else{
				 
				for($i=0;$i<count($fields_position_order);$i++){
					$sql[]=$wpdb->get_results("select * from $custom_fields_tables where sequence=$fields_position_order[$i]");			
				}
			}
			$results = array_column($sql, '0');
			return new WP_REST_Response( $sql, 200 );
	
	  }
	  /**
	   * 
	   * Return Custom template values
	   */
	  function getCustomtemplates(){

		  global $wpdb;
		  $template_table=$wpdb->prefix."ant_stand_template";
	//	  $wpdb->show_errors();
		  $sql=$wpdb->get_results("select * from $template_table");
	//	$wpdb->print_error();
	//	die;
			return new WP_REST_Response( $sql, 200 );
	  }
	  /**
	   * 
	   * Return ticket table fields
	   */
	  function filter_ticketfield(){
		global $wpdb;
		$table_name=$wpdb->prefix."ant_tickets";
		$columns=$wpdb->get_results("show columns from $table_name");
		$columns=array_values($columns);
	//	unset($columns[0]);
	//	print_r($columns);die;
	//	print_R($columns[0]->Field);die;
		for($i=0;$i<count($columns);$i++)
		{
			if($columns[$i]->Field=='id')
			{
				unset($columns[$i]);
			}
		return rest_ensure_response( array_column($columns,'Field'));		
		}
	}
		function my_cron_schedules($schedules)
		{

		   $schedules['every_three_minutes'] = array(
			   'interval' => 60,
			   'display' => esc_html__( 'Every three minute' ),
			   );
			   return $schedules;

		}
		function isa_activation() {

		if ( ! wp_next_scheduled( 'my_cron_schedules' ) ) {
		   wp_schedule_event( time(), 'every_three_minutes', 'my_cron_schedules' );
	   }
   }

	function every_three_minutes_event_func(){

		require_once plugin_dir_path( __FILE__ ).'/vendor/autoload.php';

		//print_R($date);die;
		$imap_config=get_option('imap_configure');
		$imap_date=$imap_config['date'];
		$imap_email=$imap_config['email'];
		$imap_pass=$imap_config['pass'];
		$date = new DateTime($imap_date);
		$result = $date->format('d-m-Y H:i:s');
		$date = strtotime($result);
		$date= date('d M Y ', $date);
	//	var_dump($date);die;
		$mailbox = new PhpImap\Mailbox(
			'{imap.gmail.com:993/imap/ssl}INBOX', // IMAP server and mailbox folder
			$imap_email, // Username for the before configured mailbox
			$imap_pass, // Password for the before configured username
			__DIR__."", // Directory, where attachments will be saved (optional)
			'UTF-8' // Server encoding (optional)
		);
	//	print_r($mailbox);die;
				try {

							// Get all emails (messages)
					// PHP.net imap_search criteria: http://php.net/manual/en/function.imap-search.php
					$mailsIds = $mailbox->searchMailbox('SINCE "'.trim($date,' ').'"');

				} catch(PhpImap\Exceptions\ConnectionException $ex) {
					echo "IMAP connection failed: " . $ex->getMessage();
					die();
				}
				// If $mailsIds is empty, no emails could be found
			if(!$mailsIds) {
				die('Mailbox is empty');
			}

				// Get the first message
				// If '__DIR__' was defined in the first line, it will automatically
				// save all attachments to the specified directory

				rsort($mailsIds);
				for($i=0;$i<count($mailsIds);$i++){
				$mail[]= $mailbox->getMail($mailsIds[$i]);
				}
				for($j=0;$j<count($mail);$j++){

					$imap_data[$j]['subject']=$mail[$j]->subject;
					$imap_data[$j]['from_address']=$mail[$j]->fromAddress;
					$imap_data[$j]['email_content']=$mail[$j]->textHTML;
					$imap_data[$j]['fromname']=$mail[$j]->fromName;
					update_option('imap_thread',$imap_data);
				}

	}
	function my_custom_my_account_menu_items( $items ) {
		$items = array(
			'dashboard'         => __( 'Dashboard', 'woocommerce' ),
			'orders'            => __( 'Orders', 'woocommerce' ),
			//'downloads'       => __( 'Downloads', 'woocommerce' ),
			//'edit-address'    => __( 'Addresses', 'woocommerce' ),
			//'payment-methods' => __( 'Payment Methods', 'woocommerce' ),
			'edit-account'      => __( 'Edit Account', 'woocommerce' ),
			'tickets'      => 'Tickets',
			'customer-logout'   => __( 'Logout', 'woocommerce' ),
		);

		return $items;
	}

function my_custom_flush_rewrite_rules() {
    flush_rewrite_rules();
}
function my_custom_query_vars( $vars ) {
	$vars[] = 'tickets';
    return $vars;
}
function my_custom_endpoints() {
   			add_rewrite_endpoint( 'tickets', EP_ROOT | EP_PAGES );
}
//imap configure
function imap_configure(){

	$imap_email=$_REQUEST['imapemail'];
	$imap_pass=$_REQUEST['imappass'];
	$imap_date=$_REQUEST['imapdate'];
	$data=['email'=>$imap_email,'pass'=>$imap_pass,'date'=>$imap_date];
	update_option('imap_configure',$data);
	wp_die();
}
//ajax call for save custom field value
function customfieldvaluesave(){
	global $wpdb;
	$customfields=$_REQUEST['selected_value'];
	$customlabels=$_REQUEST['fieldsvalue'];
	$custom_fields_tables=$wpdb->prefix."ant_custom_fields";
	$rowcount=$wpdb->get_var("select count(*) from $custom_fields_tables");
	$fields_position_order=get_option('customfield_position_order');
	$fields_position_order[$rowcount]=$rowcount+1;
	update_option('customfield_position_order',$fields_position_order);
	$wpdb->insert($custom_fields_tables,array('labels'=>$customlabels,'sequence'=>$rowcount+1,'fields'=>$customfields));
	wp_die();
}
function cfupdatedragchanges(){

	$changesdata=$_REQUEST['changes'];
//	print_r($changesdata);die;
	update_option('customfield_position_order',$changesdata);
//	print_R(get_option('customfield_position_order'));
  	 wp_die();
}
/**
 * 
 * Add custom template
 */
function customTemplates(){
	$mode=$_REQUEST['mode'];
	$templateName=$_REQUEST['templatename'];
	$templateContent=$_REQUEST['templatecontent'];
	$id=$_REQUEST['tempid'];
	global $wpdb;
	$template_table=$wpdb->prefix."ant_stand_template";
	switch($mode){
		case "tempsave":
			$wpdb->show_errors();
	  $wpdb->query(
		$wpdb->prepare(
			"UPDATE {$template_table} SET temp_labels='$templateName',temp_content='$templateContent' where id=$id"
		) // $wpdb->prepare
	); // $wpdb->query
	$wpdb->print_error();
	//	print_R($templates);
	wp_die();
		break;
		case "temp-delete":
		$delid=$_REQUEST['delid'];
		$wpdb->show_errors();
		$wpdb->delete( $template_table, array( 'id' => $delid ) );
		$wpdb->print_error();
		wp_die();
		break;
		case "new":
		$template_name=$_REQUEST['templatename'];
		$template_content=$_REQUEST['templatecontent'];
	//	$wpdb->show_errors();
		$wpdb->insert($template_table,array('temp_labels'=>$template_name,'temp_content'=>$template_content));
	//	$wpdb->print_error();
		$results=$wpdb->get_results("select * from $template_table order by id desc limit 1");
		echo json_encode($results);
		wp_die();
		break;
	}
	
}
}
