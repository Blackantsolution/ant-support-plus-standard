<?php

namespace PhpImap\Exceptions;

use Exception;

/**
 * @see https://github.com/barbushin/php-imap
 *
 * @subject Barbushin Sergey http://linkedin.com/in/barbushin
 */
class InvalidParameterException extends Exception
{
}
