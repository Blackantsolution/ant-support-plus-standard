(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
		//IMAP CONFIGURATION
		jQuery(document).on('click','.imapsave',function(){

			var imapemail=jQuery('#imapemail').val();
			var imappass=jQuery('#imappassword').val();
			var imapdate=jQuery('#imapdate').val();
			var data={
				action:"imap_configure",
				imapemail:imapemail,
				imappass:imappass,
				imapdate:imapdate
			}
			jQuery.post(ant_support_plus_admin.ajax_var,data,function(){
				console.log('saved')
			})
		})
			//Custom fields ///

			var multiplefield={};
			jQuery(document).on('click','.customfieldsave',function(){
				var fieldvalue=jQuery('#customlabels').val();
				var selected_value=jQuery('.selectcustomfield option:selected').val();
				var selected_text=jQuery('.selectcustomfield option:selected').text();
				multiplefield[fieldvalue]=selected_value
				if(fieldvalue == ''){
					jQuery('#customlabels').focus();
					jQuery('<p style="color:red">Empty values not allowed</p>').insertAfter('#customlabels');
					return false;
				}
				var data={
					action:"customfieldvaluesave",
					fieldsvalue:fieldvalue,
					selected_value:selected_value
				}
				jQuery.post(ant_support_plus_admin.ajax_var,data,function(){

					jQuery('<div class="ba-sp-popup bg-teal-100 border-t-4 border-teal-500 w-6/12 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert"><div class="flex"><svg class="h-6 w-6 text-teal mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg><div><p class="font-bold"></p><p class="text-sm">List changed.</p></div></div></div>').insertAfter('.aspf-customfields');
					setTimeout(function(){
						jQuery('.ba-sp-popup').fadeOut('slow');
					},1000);
				})
			})
			jQuery(document).on('click','#cfupdatechanges',function(e){
				jQuery('div.spinner-border').show();
				  //console.log(index)
				  var indexid=[];
				  jQuery('ul li.list-group-item-customfields').each(function(i)
					{
						var id=jQuery(this).attr('seq');
						indexid.push(id)
					});
					console.log(indexid)
				var data={

					action:'cfupdatedragchanges',
					changes:indexid,
				}
				jQuery.post(ant_support_plus_admin.ajax_var,data,function(response){

					jQuery('<div class="ba-sp-popup bg-teal-100 border-t-4 border-teal-500 w-6/12 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert"><div class="flex"><svg class="h-6 w-6 text-teal mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg><div><p class="font-bold"></p><p class="text-sm">List changed.</p></div></div></div>').insertAfter('.sortableList-customfields');
					setTimeout(function(){
						jQuery('.ba-sp-popup').fadeOut('slow');
					},1000);

				})
		   })
								   /* Templates*/
								   	//field value insertion to ckeditor
	jQuery(document).on('click', '.tempfieldinsert', function () {
		var insert_value = jQuery('#tempfieldselect :selected').val();
		var myvalue = insert_value.trim();
		CKEDITOR.instances.tempContent.insertText("{{" + myvalue + "}}")
	})
								   	var click=1;

		jQuery(document).on('click','.tempedit',function(event){
			event.preventDefault();
		//	jQuery('.flex-col').css({'-webkit-filter': 'blur(1px)'});
			var editid=jQuery(this).attr('edit-id');
			jQuery('.templatetable').hide();
			jQuery('.templatecreate').hide();
			jQuery('.createtemplate').hide();
			if(click == 1){
			CKEDITOR.replace('tempContent');
			click++;
			}else {
				CKEDITOR.instances.tempContent.destroy();
				CKEDITOR.replace('tempContent')
			}

			jQuery('.savetemplates').attr('temp-id',editid);
		//	jQuery('.flex-col').hide();
			jQuery('.addtemplate').show();

		//	jQuery("html, body").animate({ scrollTop: jQuery(document).height() }, "slow");
		})
		jQuery(document).on('click','.tempdelete ',function(){
			 var delid=jQuery(this).attr('del-id');
			var data={
				action:'customTemplates',
				delid:delid,
				'mode':'temp-delete'
			}
			jQuery.post(ant_support_plus_admin.ajax_var,data,function(response){
				//alert('record deleted');
				jQuery('.filter_template_table tr[del-id='+delid+']').remove();
				jQuery('<div class="ba-sp-popup bg-teal-100 border-t-4 border-teal-500 w-full rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert"><div class="flex"><svg class="h-6 w-6 text-teal mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg><div><p class="font-bold"></p><p class="text-sm">Templates was deleted successfully.</p></div></div></div>').insertAfter('ul.aspf-flex');
			//window.location=window.location.href;
					  setTimeout(function(){
						  jQuery('.ba-sp-popup').fadeOut('slow');
					  },1000);

			})
		})
		jQuery(document).on('click','#tempclose',function () {
		//	jQuery('.flex-col').css({'-webkit-filter': 'blur(0px)'});
			jQuery('.templatetable').show();
			jQuery('.addtemplate').hide();
			jQuery('.createtemplate').show();
		})

			//create custom template
			var tempclick=1;
			jQuery(document).on('click','.createtemplate',function(){
				jQuery('.create_template').show();
				jQuery('.templatetable').hide();
				jQuery(this).hide();
				if(tempclick == 1){
					CKEDITOR.replace('templatecontent');
					tempclick++;
					}else {
						CKEDITOR.instances.templatecontent.destroy();
						CKEDITOR.replace('templatecontent')
					}
			//	CKEDITOR.replace('templateontent');
			})
			//submit new tmplate
			jQuery(document).on('click','.create_ant_templates',function(){
				var templatename=jQuery('#templatename').val();
				var templatecontent=CKEDITOR.instances.templatecontent.getData();
				var data={
					action:"customTemplates",
					templatecontent:templatecontent,
					templatename:templatename,
					"mode":"new"
				}
				jQuery.post(ant_support_plus_admin.ajax_var,data,function(response){
					var Obj=JSON.parse(response)
					console.log(Obj)
					if(Obj){
						jQuery('.aspf-template tbody').append('<tr del-id='+Obj[0].id+'><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><div class="aspf-text-sm aspf-leading-5 aspf-text-gray-900">'+Obj[0].temp_labels+'</div></td><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><div class="aspf-flex aspf-items-center"><div class="aspf-ml-4"><div class="aspf-text-sm aspf-leading-5 aspf-font-medium aspf-text-gray-900">'+Obj[0].temp_content+'</div></div></div></td><td class="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200"><button class="tempedit aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline" edit-id="'+Obj[0].id+'">Edit /</button>&nbsp;<button class="tempdelete aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline" del-id="'+Obj[0].id+'">Delete </button>&nbsp;</td></tr>');
						jQuery('.create_template').hide();
						jQuery('.createtemplate').show();
						jQuery('.templatetable').show();
					}
				})
			})
			//cancel template submission
			jQuery(document).on('click','.cancel_ant_templates',function(){
						jQuery('.create_template').hide();
						jQuery('.createtemplate').show();
						jQuery('.templatetable').show();
					
			})
		   //Update custom templates
		   jQuery(document).on('click','.savetemplates',function(){
			   var templatename=jQuery('#templatelabel').val();
			   var tempid=jQuery(this).attr('temp-id');
			   var templatecontent=CKEDITOR.instances.tempContent.getData();
			   if(templatename==""){
				jQuery('#templatelabel').focus();
				jQuery('<p style="color:red">Please fill Required Field</p>').insertAfter('#templatelabel');
				   return false;
			   }
			   var data={
				   action:'customTemplates',
				   tempid:tempid,
				   templatename:templatename,
				   templatecontent:templatecontent,
				   'mode':'tempsave'
			   }

			   jQuery.post(ant_support_plus_admin.ajax_var,data,function(){

				jQuery('<div class="ba-sp-popup bg-teal-100 border-t-4 border-teal-500 w-6/12 rounded-b text-teal-900 px-4 py-3 shadow-md" role="alert"><div class="flex"><svg class="h-6 w-6 text-teal mr-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"/></svg><div><p class="font-bold"></p><p class="text-sm"> saved.</p></div></div></div>').insertAfter('.savetemplates');
				setTimeout(function(){
					jQuery('.ba-sp-popup').fadeOut('slow');
				},1000);

			})
			   })
})( jQuery );
