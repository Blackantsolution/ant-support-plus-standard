var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function ASPIMAP() {
  return React.createElement(
    "div",
    null,
    React.createElement(
      "div",
      { className: "aspf-w-full aspf-max-w-xl" },
      React.createElement(
        "form",
        { className: "aspf-rounded aspf-px-8 aspf-pt-6 aspf-pb-8 aspf-mb-4" },
        React.createElement(
          "div",
          { className: "aspf-mb-4" },
          React.createElement(
            "label",
            { className: "aspf-block aspf-text-gray-700 aspf-text-sm font-bold mb-2", htmlFor: "username" },
            "UserEmail"
          ),
          React.createElement("input", { className: "aspf-shadow aspf-appearance-none aspf-border aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline", id: "imapemail", type: "text", placeholder: "Username" })
        ),
        React.createElement(
          "div",
          { className: "aspf-mb-6" },
          React.createElement(
            "label",
            { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold aspf-mb-2", htmlFor: "password" },
            "Password"
          ),
          React.createElement("input", { className: "aspf-aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline", id: "imappassword", type: "password", placeholder: "******************" })
        ),
        React.createElement(
          "div",
          { className: "aspf-mb-2" },
          React.createElement(
            "label",
            { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold aspf-mb-2", htmlFor: "password" },
            "From Date"
          ),
          React.createElement("input", { className: "aspf-aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline", id: "imapdate", type: "date", placeholder: "******************" })
        ),
        React.createElement(
          "div",
          { className: "aspf-flex aspf-items-center aspf-justify-between" },
          React.createElement(
            "button",
            { className: "imapsave aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline", type: "button" },
            "Save"
          )
        )
      )
    )
  );
}
function ASPCField() {
  return React.createElement(
    "div",
    { className: "aspf-customfields" },
    React.createElement(
      "div",
      { className: "aspf-subject" },
      React.createElement(
        "label",
        { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "username" },
        "Add Labels"
      ),
      React.createElement("input", { type: "text", id: "customlabels", className: "aspf-w-6/12" })
    ),
    React.createElement(
      "div",
      { className: "aspf-inline-block aspf-pt-4 aspf-relative aspf-w-64 ba-sp-customfields" },
      React.createElement(
        "label",
        { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "username" },
        "Select Fields"
      ),
      React.createElement(
        "select",
        { className: "selectcustomfield aspf-block aspf-appearance-none aspf-w-full aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 aspf-pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" },
        React.createElement(
          "option",
          { value: "none" },
          "None"
        ),
        React.createElement(
          "option",
          { value: "ba-text" },
          "Text"
        ),
        React.createElement(
          "option",
          { value: "ba-checkbox" },
          "Checkbox"
        ),
        React.createElement(
          "option",
          { value: "ba-drop" },
          "Drodown"
        ),
        React.createElement(
          "option",
          { value: "ba-textarea" },
          "Textarea"
        )
      )
    ),
    React.createElement("br", null),
    React.createElement("div", { className: "aspf-mt-4", id: "multiplefield" }),
    React.createElement(
      "button",
      { className: "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline customfieldsave", type: "button" },
      "Save"
    )
  );
}
function ASPLayout() {

  var aspf_endpoint = ant_support_plus_admin.url + 'wp-json/ant-support-plus-standard/v1/blackant';

  var _React$useState = React.useState([]),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      customfields = _React$useState2[0],
      setCustomField = _React$useState2[1];

  React.useEffect(function () {
    if (Object.keys(customfields).length == 0) {
      fetch(aspf_endpoint + '/customfields').then(function (res) {
        return res.json();
      }).then(function (data) {
        //  console.log(data)
        setCustomField(data);
      });
    }
  });
  var customfieldslist = customfields.map(function (data) {
    return data.map(function (cfdata, index) {
      //  console.log(cfdata);return false;
      if (cfdata.fields == 'ba-checkbox') {
        return React.createElement(
          "div",
          { key: index },
          React.createElement(
            "div",
            { className: "aspf-mb-6" },
            React.createElement(
              "label",
              { className: "aspf-w-2/3 aspf-block aspf-text-gray-500 aspf-font-bold" },
              React.createElement("input", { className: "aspf-mr-2 aspf-leading-tight", type: "checkbox" }),
              React.createElement(
                "span",
                { className: "aspf-text-sm" },
                cfdata.labels
              )
            )
          )
        );
      } else if (cfdata.fields == 'ba-text') {
        return React.createElement(
          "div",
          { key: index },
          React.createElement(
            "div",
            { className: "aspf-subject" },
            React.createElement(
              "label",
              { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "username" },
              cfdata.labels
            ),
            React.createElement("input", { type: "text", id: "subject" })
          )
        );
      } else if (cfdata.fields == 'ba-drop') {
        return React.createElement(
          "div",
          { key: index },
          React.createElement(
            "div",
            null,
            React.createElement(
              "select",
              null,
              React.createElement(
                "option",
                null,
                "1"
              )
            )
          )
        );
      } else if (cfdata.fields == 'ba-textarea') {
        return React.createElement(
          "div",
          { key: index, className: "aspf-mb-6" },
          React.createElement(
            "label",
            { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "password" },
            cfdata.labels
          ),
          React.createElement("textarea", { className: "aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline", id: "ticketdesc" })
        );
      }
    });
  });
  return React.createElement(
    "div",
    { className: "aspf-mt-4" },
    React.createElement(
      "ul",
      { className: "sortableList list-group" },
      customfields.map(function (data) {
        return data.map(function (cfdata, index) {
          return React.createElement(
            "li",
            { key: index, cfkeys: cfdata.labels, id: cfdata.id, seq: cfdata.sequence, cfvalues: cfdata.fields, className: "aspf-bg-gray-100 ba-sp-dragli ui-state-default list-group-item-customfields list-none sm:list-disc md:list-decimal lg:list-disc xl:list-none", style: { width: "50%" } },
            cfdata.labels,
            React.createElement(
              "span",
              { style: { float: 'right' } },
              React.createElement("span", { className: "dashicons dashicons-menu-alt3", "aria-hidden": "true" })
            )
          );
        });
      })
    ),
    React.createElement("input", { name: "save", id: "cfupdatechanges", className: "aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded", type: "button", value: "Save Changes" }),
    customfieldslist
  );
}

function ASPTemplates() {

  var aspf_endpoint = ant_support_plus_admin.url + 'wp-json/ant-support-plus-standard/v1/blackant';

  var _React$useState3 = React.useState([]),
      _React$useState4 = _slicedToArray(_React$useState3, 2),
      template = _React$useState4[0],
      setTemplate = _React$useState4[1];

  var _React$useState5 = React.useState(''),
      _React$useState6 = _slicedToArray(_React$useState5, 2),
      tempname = _React$useState6[0],
      setTempname = _React$useState6[1];

  var _React$useState7 = React.useState(''),
      _React$useState8 = _slicedToArray(_React$useState7, 2),
      tempcontent = _React$useState8[0],
      setTempcontent = _React$useState8[1];

  var _React$useState9 = React.useState([]),
      _React$useState10 = _slicedToArray(_React$useState9, 2),
      mailstatus = _React$useState10[0],
      setMailstatus = _React$useState10[1];

  React.useEffect(function () {
    if (Object.keys(template).length == 0) {
      fetch(aspf_endpoint + '/customtemplates').then(function (res) {
        return res.json();
      }).then(function (data) {
        setTemplate(data);
      }).then(function () {
        if (Object.keys(mailstatus).length == 0) {
          fetch(aspf_endpoint + '/filter_ticketfield').then(function (res) {
            return res.json();
          }).then(function (result) {
            console.log(result);
            setMailstatus(result);
          }, function (error) {
            // Error occured
            console.log(error);
          });
        }
      });
    }
  });
  var fieldlabels = { '1': 'TICKET ID', '2': 'SUBJECT', '3': 'DESCRIPTION', '4': 'TICKET STATUS', '5': 'TICKET PRIOROTY', '6': 'TICKET CATEGORY', '7': 'CUSTOMER ID', '8': 'DUE DATE', '9': 'TICKET SOURCE', '10': 'MORE INFO', '11': 'ASSIGNED TO', '12': 'CREATED BY', '13': 'CREATOR TYPE', '14': 'CREATED AT', '15': 'UPDATED AT' };
  var mailstatuskeys = Object.keys(mailstatus);
  function tempdetail(index) {
    var tempdetail = template[index];
    setTempname(tempdetail.temp_labels);
    setTempcontent(tempdetail.temp_content);
  }
  function tempnameChange(e) {
    setTempname(e.target.value);
  }
  function tempcontentChange(e) {
    setTempcontent(e.target.value);
  }
  return React.createElement(
    "div",
    null,
    React.createElement(
      "button",
      { className: "aspf-float-right aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded createtemplate", type: "button" },
      "Add Template"
    ),
    React.createElement(
      "div",
      { className: "aspf-my-2 aspf-py-2 aspf-overflow-x-auto aspf-sm:-mx-6 aspf-sm:px-6 lg:-mx-8 aspf-lg:px-8 aspf-mt-8 templatetable " },
      React.createElement(
        "div",
        { className: "aspf-align-middle aspf-inline-block aspf-min-w-full aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg aspf-border-b aspf-border-gray-200" },
        React.createElement(
          "table",
          { className: "aspf-min-w-full aspf-template" },
          React.createElement(
            "thead",
            null,
            React.createElement(
              "tr",
              null,
              React.createElement(
                "th",
                { className: "aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider" },
                "Template Name"
              ),
              React.createElement(
                "th",
                { className: "aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider" },
                "Template Content"
              ),
              React.createElement(
                "th",
                { className: "aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider" },
                "Actions"
              )
            )
          ),
          React.createElement(
            "tbody",
            { className: "aspf-bg-white filter_template_table" },
            template.map(function (data, index) {
              //  console.log(data)
              return React.createElement(
                "tr",
                { key: index, "del-id": data.id },
                React.createElement(
                  "td",
                  { className: "aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200" },
                  React.createElement(
                    "div",
                    { className: "aspf-text-sm aspf-leading-5 aspf-text-gray-900" },
                    data.temp_labels
                  )
                ),
                React.createElement(
                  "td",
                  { className: "aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200" },
                  React.createElement(
                    "div",
                    { className: "aspf-flex aspf-items-center" },
                    React.createElement(
                      "div",
                      { className: "aspf-ml-4" },
                      React.createElement(
                        "div",
                        { className: "aspf-text-sm aspf-leading-5 aspf-font-medium aspf-text-gray-900" },
                        React.createElement("p", { dangerouslySetInnerHTML: { __html: data.temp_content } })
                      )
                    )
                  )
                ),
                React.createElement(
                  "td",
                  { className: "aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200" },
                  React.createElement(
                    "button",
                    { className: "tempedit aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline", "edit-id": data.id, onClick: function onClick() {
                        return tempdetail(index);
                      } },
                    "Edit /"
                  ),
                  "\xA0",
                  React.createElement(
                    "button",
                    { className: "tempdelete aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline", "del-id": data.id },
                    "Delete "
                  ),
                  "\xA0"
                )
              );
            })
          )
        )
      )
    ),
    React.createElement(
      "div",
      { className: "addtemplate aspf-hidden" },
      React.createElement(
        "button",
        { className: "btn", type: "button", id: "tempclose", style: { float: 'right' } },
        React.createElement("span", { className: "dashicons dashicons-no" })
      ),
      "\xA0",
      React.createElement(
        "div",
        { className: "aspf-template" },
        React.createElement(
          "label",
          { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "username" },
          "Template Name\xA0",
          React.createElement(
            "span",
            { style: { color: "red" } },
            "*"
          )
        ),
        React.createElement("input", { type: "text", id: "templatelabel", className: "aspf-w-6/12", value: tempname, onChange: tempnameChange })
      ),
      React.createElement(
        "div",
        { className: "aspf-mb-6" },
        React.createElement(
          "select",
          { className: "tempinsert aspf-block aspf-appearance-none aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline", id: "tempfieldselect" },
          React.createElement("option", null),
          mailstatus.map(function (data, index) {
            return React.createElement(
              "option",
              { key: index, value: data },
              fieldlabels[index + 1]
            );
          })
        ),
        React.createElement("br", null),
        React.createElement(
          "button",
          { type: "button", className: "aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline tempfieldinsert" },
          "insert"
        )
      ),
      React.createElement(
        "div",
        { className: "aspf-mb-6" },
        React.createElement(
          "label",
          { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "password" },
          "Template Content"
        ),
        React.createElement("textarea", { className: "aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline", name: "tempContent", id: "tempContent", value: tempcontent, onChange: tempcontentChange })
      ),
      React.createElement("input", { name: "save", className: "savetemplates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded", type: "button", value: "update" })
    ),
    React.createElement(
      "div",
      { className: "create_template aspf-hidden" },
      React.createElement(
        "div",
        { className: "aspf-template" },
        React.createElement(
          "label",
          { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "username" },
          "Template Name\xA0",
          React.createElement(
            "span",
            { style: { color: "red" } },
            "*"
          )
        ),
        React.createElement("input", { type: "text", id: "templatename", className: "aspf-w-6/12" })
      ),
      React.createElement(
        "div",
        { className: "aspf-mb-6" },
        React.createElement(
          "label",
          { className: "aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2", htmlFor: "password" },
          "Template Content"
        ),
        React.createElement("textarea", { className: "aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline", name: "templatecontent", id: "templatecontent" })
      ),
      React.createElement(
        "span",
        null,
        React.createElement("input", { name: "save", className: "create_ant_templates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded", type: "button", value: "Save" }),
        "\xA0\xA0",
        React.createElement("input", { name: "save", className: "cancel_ant_templates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded", type: "button", value: "Cancel" })
      )
    )
  );
}