'use strict';
function TicketsListview() {
  const aspf_endpoint = ant_support_plus_standard_public.site_url + 'wp-json/ant-support-plus-standard/v1/blackant';
  const customer_id = btoa(ant_support_plus_standard_public.customer_id);
  const [tablefields, setTableField] = React.useState([]);
  const [customertickets, setCustomertikets] = React.useState([]);
  const [priority, setPriority] = React.useState([]);
  React.useEffect(() => {
    if (Object.keys(tablefields).length == 0) {
      fetch(aspf_endpoint + "/tablefields").then(res => res.json()).then(data => {
        setTableField(data);
      }).then(() => {
        if (Object.keys(customertickets).length == 0) {
          fetch(aspf_endpoint + "/customertickets?customer_id=" + customer_id).then(res => res.json()).then(data => {
            //   console.log(data)
            setCustomertikets(data);
          })
        }
      }).then(() => {
        if (Object.keys(priority).length == 0) {
          fetch(aspf_endpoint + '/ticketpriority').then(res => res.json()).then((data) => {
            //  console.log(data)
            setPriority(data)
          }, (error) => {
            // Error occured
            console.log(error);
          })
        }
      })
    }
  })
  return (
    <div className="aspf-my-2 aspf-py-2 aspf-overflow-x-auto aspf-sm:-mx-6 aspf-sm:px-6 aspf-lg:-mx-8 aspf-lg:px-8 " id='ant_tickets'>
      <div className="aspf-align-middle aspf-inline-block aspf-min-w-full aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg aspf-border-b aspf-border-gray-200">
        <button className="aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline aspf-float-right aspf-addticket" type="button">
          Add Ticket
                    </button>
        <table className="aspf-min-w-full" id='customer_tickets'>
          <thead>
            <tr>
              {tablefields.map((data, index) => {
                return (
                  <th key={index} className="aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-2xl aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider">
                    {data}
                  </th>
                )
              })}
            </tr>
          </thead>
          <tbody className="bg-white filter_table">
            {customertickets.map((data, index) => {
              //   console.log(data)
              return (
                <tr key={index}>
                  <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                    <div className="aspf-text-2xl aspf-leading-5 aspf-text-gray-900"><a href='#' ticket_id={data.id} id='listview'>{data.subject}</a></div>
                  </td>
                  <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                    <div className="aspf-text-2xl aspf-leading-5 aspf-text-gray-900">{data.ticket_status}</div>
                  </td>
                  <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                    <div className="aspf-text-2xl aspf-leading-5 aspf-text-gray-900">{data.ticket_priority}</div>
                  </td>
                </tr>
              )
            })}

          </tbody>
        </table>
      </div>
      {/*Form for new ticket creation*/}
      <form className="aspf-hidden aspf-bg-white aspf-shadow-md aspf-rounded aspf-px-8 aspf-pt-6 aspf-pb-8 aspf-mb-4 ticketsubmit">
        <div className="aspf-mb-6">
          <label className="aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold mb-2" htmlFor="subject">Subject</label>
          <input className="aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded aspf-w-6/12 aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id="ticketsubject" type="text" />
        </div>

        <div className='aspf-inline-block aspf-pt-4 aspf-relative aspf-w-64'>
          <label className="aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold mb-2" htmlFor="username">Ticket Priority</label>
          <select className="aspf-block aspf-appearance-none aspf-w-full aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 aspf-pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id='ant-ticketpriority' >
            {priority.map((data, index) => {
              return (
                <option value={'ba-sp-' + data.name} key={index}>{data.name}</option>
              )
            })}
          </select>
        </div><br></br>

        <div className="aspf-mb-6">
          <label className="aspf-block aspf-text-gray-700 aspf-text-3xl aspf-font-bold aspf-mb-2" htmlFor="password">Description</label>
          <textarea className="aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" id="ant_standard_publicticketdescription" name="ant_standard_publicticketdescription" ></textarea>
        </div>

        <div className="aspf-mt-4 ">
          <button className="submit_public_ticket aspf-bg-blue-500 aspf-hover:bg-blue-700 vtext-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" type="button" >Submit</button>&nbsp;
                         <button className="cancel_public_ticket aspf-bg-blue-500 aspf-hover:bg-blue-700 vtext-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline " type="button">Cancel</button>&nbsp;
                         </div>

      </form>
    </div>
  );
}
jQuery(document).ready(function () {
  let domContainer = document.querySelector('.woocommerce-MyAccount-content');
  var url = window.location.href.indexOf('tickets');
  if (domContainer && url > -1) {
    ReactDOM.render(<TicketsListview />, domContainer);
  }
})