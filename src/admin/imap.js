function ASPIMAP() {
  return (
    <div>
      <div className="aspf-w-full aspf-max-w-xl">
        <form className="aspf-rounded aspf-px-8 aspf-pt-6 aspf-pb-8 aspf-mb-4">
          <div className="aspf-mb-4">
            <label className="aspf-block aspf-text-gray-700 aspf-text-sm font-bold mb-2" htmlFor="username">
              UserEmail
      </label>
            <input className="aspf-shadow aspf-appearance-none aspf-border aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id="imapemail" type="text" placeholder="Username" />
          </div>
          <div className="aspf-mb-6">
            <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold aspf-mb-2" htmlFor="password">
              Password
      </label>
            <input className="aspf-aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id="imappassword" type="password" placeholder="******************" />
          </div>
          <div className="aspf-mb-2">
            <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold aspf-mb-2" htmlFor="password">
              From Date
      </label>
            <input className="aspf-aspf-shadow aspf-appearance-none aspf-border aspf-border-red-500 aspf-rounded w-full aspf-py-2 aspf-px-3 aspf-text-gray-700 aspf-mb-3 aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id="imapdate" type="date" placeholder="******************" />
          </div>
          <div className="aspf-flex aspf-items-center aspf-justify-between">
            <button className="imapsave aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" type="button">
              Save
      </button>
          </div>
        </form>

      </div>
    </div>
  )
}
function ASPCField() {
  return (
    <div className="aspf-customfields">

      <div className='aspf-subject'>
        <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="username">Add Labels</label>
        <input type='text' id='customlabels' className='aspf-w-6/12' />
      </div>
      <div className="aspf-inline-block aspf-pt-4 aspf-relative aspf-w-64 ba-sp-customfields">
        <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="username">Select Fields</label>
        <select className="selectcustomfield aspf-block aspf-appearance-none aspf-w-full aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 aspf-pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline">
          <option value='none'>None</option>
          <option value='ba-text'>Text</option>
          <option value='ba-checkbox'>Checkbox</option>
          <option value='ba-drop'>Drodown</option>
          <option value='ba-textarea'>Textarea</option>
        </select>
      </div><br></br>

      <div className='aspf-mt-4' id='multiplefield'></div>
      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline customfieldsave" type="button"  >Save</button>
    </div>
  )
}
function ASPLayout() {

  const aspf_endpoint = ant_support_plus_admin.url + 'wp-json/ant-support-plus-standard/v1/blackant';
  const [customfields, setCustomField] = React.useState([]);
  React.useEffect(() => {
    if (Object.keys(customfields).length == 0) {
      fetch(aspf_endpoint + '/customfields').then(res => res.json()).then(data => {
        //  console.log(data)
        setCustomField(data)
      })
    }
  })
  var customfieldslist = customfields.map(data => {
    return data.map((cfdata, index) => {
      //  console.log(cfdata);return false;
      if (cfdata.fields == 'ba-checkbox') {
        return (
          <div key={index}>
            <div className="aspf-mb-6">
              <label className="aspf-w-2/3 aspf-block aspf-text-gray-500 aspf-font-bold">
                <input className="aspf-mr-2 aspf-leading-tight" type="checkbox" />
                <span className="aspf-text-sm">
                  {cfdata.labels}
                </span>
              </label>
            </div>
          </div>
        )
      } else if (cfdata.fields == 'ba-text') {
        return (
          <div key={index}>
            <div className='aspf-subject'>
              <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="username">{cfdata.labels}</label>
              <input type='text' id='subject' />
            </div>
          </div>
        )
      } else if (cfdata.fields == 'ba-drop') {
        return (
          <div key={index}>
            <div>
              <select>
                <option>1</option>
              </select>
            </div>
          </div>
        )
      } else if (cfdata.fields == 'ba-textarea') {
        return (
          <div key={index} className="aspf-mb-6">
            <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="password">{cfdata.labels}</label>
            <textarea className="aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" id="ticketdesc"></textarea>
          </div>

        )
      }

    })
  })
  return (
    <div className="aspf-mt-4">
      <ul className='sortableList list-group'>
        {customfields.map((data) => {
          return data.map((cfdata, index) => {
            return (
              <li key={index} cfkeys={cfdata.labels} id={cfdata.id} seq={cfdata.sequence} cfvalues={cfdata.fields} className='aspf-bg-gray-100 ba-sp-dragli ui-state-default list-group-item-customfields list-none sm:list-disc md:list-decimal lg:list-disc xl:list-none' style={{ width: "50%" }}>{cfdata.labels}
                <span style={{ float: 'right' }}>
                  <span className="dashicons dashicons-menu-alt3" aria-hidden="true"></span>
                </span>
              </li>
            )
          })
        })}
      </ul>
      <input name="save" id="cfupdatechanges" className="aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded" type="button" value="Save Changes" />
      {customfieldslist}
    </div>)
}

function ASPTemplates() {

  const aspf_endpoint = ant_support_plus_admin.url + 'wp-json/ant-support-plus-standard/v1/blackant';
  const [template, setTemplate] = React.useState([]);
  const [tempname, setTempname] = React.useState('');
  const [tempcontent, setTempcontent] = React.useState('')
  const [mailstatus,setMailstatus]=React.useState([]);

  React.useEffect(() => {
    if (Object.keys(template).length == 0) {
      fetch(aspf_endpoint + '/customtemplates').then(res => res.json()).then(data => {
        setTemplate(data)
      }).then(()=>{
        if(Object.keys(mailstatus).length == 0){
          fetch(aspf_endpoint + '/filter_ticketfield').then(res => res.json()).then((result) => {
              console.log(result);
              setMailstatus(result);
            },(error) => {
              // Error occured
              console.log(error);
    })
  }
      })
    }
  })
 const fieldlabels={'1':'TICKET ID','2':'SUBJECT','3':'DESCRIPTION','4':'TICKET STATUS','5':'TICKET PRIOROTY','6':'TICKET CATEGORY','7':'CUSTOMER ID','8':'DUE DATE','9':'TICKET SOURCE','10':'MORE INFO','11':'ASSIGNED TO','12':'CREATED BY','13':'CREATOR TYPE','14':'CREATED AT','15':'UPDATED AT'};  
 const mailstatuskeys=Object.keys(mailstatus);
 function tempdetail(index) {
    var tempdetail = template[index];
    setTempname(tempdetail.temp_labels);
    setTempcontent(tempdetail.temp_content);
  }
  function tempnameChange(e) {
    setTempname(e.target.value)
  }
  function tempcontentChange(e) {
    setTempcontent(e.target.value)
  }
  return (
    <div>
      <button className='aspf-float-right aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded createtemplate' type='button' >Add Template</button>
      <div className="aspf-my-2 aspf-py-2 aspf-overflow-x-auto aspf-sm:-mx-6 aspf-sm:px-6 lg:-mx-8 aspf-lg:px-8 aspf-mt-8 templatetable ">
        <div className="aspf-align-middle aspf-inline-block aspf-min-w-full aspf-shadow aspf-overflow-hidden aspf-sm:rounded-lg aspf-border-b aspf-border-gray-200">
          <table className="aspf-min-w-full aspf-template">
            <thead>
              <tr>
                <th className="aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider">
                  Template Name
              </th>
                <th className="aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider">
                  Template Content
              </th>
                <th className="aspf-px-6 aspf-py-3 aspf-border-b aspf-border-gray-200 aspf-bg-gray-50 aspf-text-left aspf-text-xs aspf-leading-4 aspf-font-medium aspf-text-gray-500 aspf-uppercase aspf-tracking-wider">
                  Actions
                </th>
              </tr>
            </thead>
            <tbody className="aspf-bg-white filter_template_table">
              {template.map((data, index) => {
                //  console.log(data)
                return (
                  <tr key={index} del-id={data.id}>
                    <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                      <div className="aspf-text-sm aspf-leading-5 aspf-text-gray-900">{data.temp_labels}</div>
                    </td>
                    <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                      <div className="aspf-flex aspf-items-center">
                        <div className="aspf-ml-4">
                          <div className="aspf-text-sm aspf-leading-5 aspf-font-medium aspf-text-gray-900"><p dangerouslySetInnerHTML={{ __html: data.temp_content }} /></div>
                        </div>
                      </div>
                    </td>
                    <td className="aspf-px-6 aspf-py-4 aspf-whitespace-no-wrap aspf-border-b aspf-border-gray-200">
                      <button className="tempedit aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline" edit-id={data.id} onClick={() => tempdetail(index)}>Edit /</button>&nbsp;
              <button className="tempdelete aspf-text-indigo-600 aspf-hover:text-indigo-900 aspf-focus:outline-none aspf-focus:underline" del-id={data.id}>Delete </button>&nbsp;
              </td>
                  </tr>
                )
              })}

            </tbody>
          </table>
        </div>
      </div>

      <div className="addtemplate aspf-hidden">
        <button className="btn" type="button" id="tempclose" style={{ float: 'right' }}><span className="dashicons dashicons-no"></span></button>&nbsp;
          <div className='aspf-template'>
          <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="username">Template Name&nbsp;<span style={{ color: "red" }}>*</span></label>
          <input type='text' id='templatelabel' className='aspf-w-6/12' value={tempname} onChange={tempnameChange} />
        </div>

        <div className='aspf-mb-6'>
        <select className="tempinsert aspf-block aspf-appearance-none aspf-bg-white aspf-border aspf-border-gray-400 aspf-hover:border-gray-500 aspf-px-4 aspf-py-2 pr-8 aspf-rounded aspf-shadow aspf-leading-tight aspf-focus:outline-none aspf-focus:shadow-outline" id='tempfieldselect'>
       <option></option>
        {mailstatus.map((data,index)=>{
                      return(
                        <option key={index} value={data} >{fieldlabels[index+1]}</option>
                      )
                    })}
    </select><br></br>

      <button type='button' className="aspf-bg-blue-500 aspf-hover:bg-blue-700 aspf-text-white aspf-font-bold aspf-py-2 aspf-px-4 aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline tempfieldinsert" >insert</button>
        </div>
        <div className="aspf-mb-6">
          <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="password">
            Template Content
                    </label>
          <textarea className="aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" name="tempContent" id="tempContent" value={tempcontent} onChange={tempcontentChange}></textarea>
        </div>
        <input name="save" className="savetemplates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded" type="button" value="update" />
      </div>

      <div className="create_template aspf-hidden">

        <div className='aspf-template'>
          <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="username">Template Name&nbsp;<span style={{ color: "red" }}>*</span></label>
          <input type='text' id='templatename' className='aspf-w-6/12' />
        </div>

        <div className="aspf-mb-6">
          <label className="aspf-block aspf-text-gray-700 aspf-text-sm aspf-font-bold mb-2" htmlFor="password">
            Template Content
                    </label>
          <textarea className="aspf-resize aspf-border aspf-rounded aspf-focus:outline-none aspf-focus:shadow-outline" name="templatecontent" id="templatecontent" ></textarea>
        </div>
        <span>
          <input name="save" className="create_ant_templates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded" type="button" value="Save" />&nbsp;&nbsp;
                <input name="save" className="cancel_ant_templates aspf-bg-transparent aspf-hover:bg-blue-500 aspf-text-blue-700 aspf-font-semibold aspf-hover:text-white aspf-py-2 aspf-px-4 aspf-border aspf-border-blue-500 aspf-hover:border-transparent aspf-rounded" type="button" value="Cancel" />
        </span>
      </div>
    </div>
  )
}

